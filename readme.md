```
$ curl -sS https://getcomposer.org/installer | php 

// 找到 composer.phar 变成全局的命令
$ mv composer.phar /usr/local/bin/composer

// 验证是否成功
$ composer -V
Composer version 1.5.2 2017-09-11 16:59:25
```
创建 composer.json

```
$ composer init
```
![这里写图片描述](http://img.blog.csdn.net/20171129175632096?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTYxNDI4NTE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)


打开文件查看 composer.json
```
 {
    "name": "xiaoliu/test",
    "description": "描述",
    "license": "MIT",
    "authors": [
        {
          "name": "xiaoliu",
            "email": "490185870@qq.com"
      }
     ],
   "require": {}
 }

那我们这个项目的名称就是xiaoliu/test  下面做的就是发布我们这个包了    
```
## 发布我们自己的package 
```
需要的账号
https://gitee.com/ 	码云
https://packagist.org  Packagist
```
```
1、码云上创建一个新的项目 [https://gitee.com/liulonghai/test.git]
2、git clone https://gitee.com/liulonghai/test.git [远程下载]
```
把composer.json 上传到码云上
```
$ mv composer.json ./test/ 
$ cd test
$ git add composer.json
$ git commit -m 'composer.json'
$ git push origin master
```
配置 Packageist
```
# 复制项目的地址 https://gitee.com/liulonghai/test.git
# 登录 Packageist 点击昵称左边的 submit
```
![提交的第一步](http://img.blog.csdn.net/20171129181928485?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTYxNDI4NTE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
![提交的最后一步](http://img.blog.csdn.net/20171129181947420?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTYxNDI4NTE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

码云 发行版本
![发行版本](http://img.blog.csdn.net/20171129182323854?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTYxNDI4NTE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
![发行配置 1.0版本](http://img.blog.csdn.net/20171129182415989?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTYxNDI4NTE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

packagist 更新版本
![更新最新的代码包](http://img.blog.csdn.net/20171129182552031?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMTYxNDI4NTE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)


## 通过composer 下载我们发行的包
```
我的包是 xiaoliu/test
$ mkdir Mytest
$ cd Mytest
$ composer require xiaoliu/test [后面这里可以跟着版本号]
   如 composer require xiaoliu/test 2.0 版本号不存在的时候会报错。packagist  update 更新会有所延迟 特别是版本更新频繁
$ ll //查看我们更新的文件
	- composer.json 这个是配置文件
	- verdor 所有的包都会在这个目录下
	- composer.look 一个锁文件
$ cat composer.json //可以看出加载我们自定义的test包
	{
	    "require": {
	        "xiaoliu/test": "1.0"
	    }
	}
```

## 真正的来开发我们自己的项目包
回到我们 test 目录
```
$ mkdir src
$ cd src
$ vim my.php
$ vim composer.json
```
my.php
```
<?php
    namespace My\test;
    class my{
        public function __construct(){
            echo 'my_construct';
        }
    }
```
composer.json 添加 autoload
```
{
    "name": "xiaoliu/test",
    "description": "描述",
    "license": "MIT",
    "authors": [
        {
            "name": "xiaoliu",
            "email": "490185870@qq.com"
        }
    ],
    "autoload": {
        "psr-4":{"My\test":"src"}
    },
    "require": {}
}

```

上传码云修改的这两个文件

重复上面的步骤 
1、码云上发布一个新的版本 
2、packagist update 
3、进入Mytest 执行 composer require xiaoliu/test  2.0【你发行的版本号】
4、创建文件 index.php
index.php
```
<?php
    $log = require "vendor/autoload.php";
    // var_dump($log);
    echo PHP_EOL;
    new My\test\my(); [My\test 是我们上传 composer.json 配置的psr-4]
```
5、php  index.php
```
my_construct 
```
### 上面的就是一个简单的列子 主要就是 
```
require "vendor/autoload.php"; 导入自动加载
使用空间命名 调用自己上传的工具类
```

